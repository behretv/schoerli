# Schoerli

 In this project the data from [Schoerli Google Analytics](https://analytics.google.com/analytics/web/?authuser=1#/report-home/a164354134w229955704p216480310), gathered via pyhton API, is used to analyze customer behaviour. If you want to analyze the data from your own webside, you would need to setup a service account (described in section *Google API*).

## Google API 
A list of useful links:
- In the following link the set-up for the python API is explained:
[QuickStart Python API](https://developers.google.com/analytics/devguides/reporting/core/v4/quickstart/service-py)
- The service account can be managed here: [Schoerli Google Cloud Console](https://console.cloud.google.com/iam-admin/serviceaccounts?project=schoerlianalytics)
- Analytics web account [Schoerli Google Analytics](https://analytics.google.com/analytics/web/?authuser=1#/report-home/a164354134w229955704p216480310)
- Query explorer for data gathering [Query
  Explorer](https://ga-dev-tools.appspot.com/query-explorer/)

## Dependencies
First of all the virtual environment should be setuop:
```bash
python3 -m venv venv/
```

To activate the virtual environment the command listed below can
be used if you use the fish shell:
```bash
. /venv/bin/activate.fish
```
und to deactivate just type
```bash
deactivate
```

All dependencies from the *requirement.txt* file can be installed by executing the following command:
```bash
pip install --upgrade pip
pip install -r requirements.txt
```

## Testing
The tests can be run locally by executing the test shell script:
```bash
./test_all.sh
```
