""" Main function to execute program """

import json
import matplotlib
import matplotlib.pyplot as plt

from src.google_analytics import GoogleAnalytics
from resources.resource_manager import ResourceManager
from src.visitor_prediction import VisitorPrediction

matplotlib.use('WebAgg')
SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
KEY_FILE_LOCATION = 'resources/configs/client_secrets.json'
VIEW_ID = '216480310'


def main():
    """ Main function to execute program """
    # Load json file from resource manager
    with open(ResourceManager().get_report_date_table()) as json_file:
        data = json.load(json_file)

    # Instantiate google analytics API
    client_secrets = ResourceManager().get_client_secrets()
    google_analytics_api = GoogleAnalytics(SCOPES, client_secrets, VIEW_ID, data)
    google_analytics_api.print_parameters()
    google_analytics_api.init_service()
    google_analytics_api.fetch_response()
    print(google_analytics_api.get_data_frame())

    # Use data fetched via the API to predict the amount of visitors
    visitor_prediction = VisitorPrediction(google_analytics_api.get_data_frame())
    visitor_prediction.plot_raw_data()
    plt.show()


if __name__ == '__main__':
    main()
