""" This manager is made to handle the files within the resources/ folder """

import pkg_resources


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class ResourceManager(metaclass=Singleton):
    """ This class manages the resources, including the correct path handling """

    def __init__(self):
        """ Virtually private constructor. """
        # Safe resources in pkg_resources
        self.client_secrets = pkg_resources.resource_filename(__name__, "configs/client_secrets.json")
        self.wrong_client_secrets = pkg_resources.resource_filename(__name__, "configs/wrong_client_secrets.json")
        self.report_user_table = pkg_resources.resource_filename(__name__, "reports/user_table.json")
        self.report_date_table = pkg_resources.resource_filename(__name__, "reports/date_table.json")
        ResourceManager.__instance = self

    def get_client_secrets(self):
        """ Getter for the client secrets file """
        return self.client_secrets

    def get_wrong_client_secrets(self):
        """ Getter for wrong client secrets for testing """
        return self.wrong_client_secrets

    def get_report_user_table(self):
        """ Getter for the reports request for user over time """
        return self.report_user_table

    def get_report_date_table(self):
        """ Getter for all kind of data over time """
        return self.report_date_table
