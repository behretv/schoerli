from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='schoerli',
    version='1.0.0',
    url='https://gitlab.com/behretv/schoerli',
    license=license,
    author='Valentino Behret',
    author_email='valentino.behret@mailbox.org',
    description='Analytics for schoerli user interaction',
    long_description=readme,
    packages=find_packages()
)
