import pandas as pd
from sklearn.model_selection import train_test_split

DATE_COLUMN = 'ga:date'


class DataHandler:
    """ Class to handle data saved in an data frame for machine learning application """
    __time = None
    __data = None

    def __init__(self, data: pd.DataFrame):
        self.__data_frame = data

        if DATE_COLUMN in data:
            self.__time = pd.to_datetime(
                self.data_frame[DATE_COLUMN], format="%Y%m%d", errors="coerce"
            )

    @property
    def data_frame(self):
        assert self.__data_frame is not None
        return self.__data_frame

    @property
    def time(self):
        return self.__time

    @property
    def predictions(self):
        return self.__predictions

    @property
    def columns(self):
        assert self.__columns is not None
        return self.__columns

    def x_train(self):
        assert "x_train" in self.__data
        return self.__data["x_train"]

    def y_train(self):
        assert "y_train" in self.__data
        return self.__data["y_train"]

    def x_test(self):
        assert "x_test" in self.__data
        return self.__data["x_test"]

    def y_test(self):
        assert "y_test" in self.__data
        return self.__data["y_test"]

    def split_data(self):
        result = False
        if self.time is not None and "ga:users" in self.data_frame:
            # Extract x and y values from data frame
            x = pd.DataFrame((self.time - self.time[0]).dt.days)
            y = self.data_frame["ga:users"]

            # Split the x and y values in training and test data to be able to
            # compute the error
            x_train, x_test, y_train, y_test = train_test_split(
                x, y, test_size=0.33, random_state=42
            )

            if (
                x_train.isnull().values.any()
                or y_train.isnull().values.any()
                or x_test.isnull().values.any()
                or y_test.isnull().values.any()
                or self.__data is None
            ):
                result = False
            else:
                self.__data["x_train"] = x_train
                self.__data["y_train"] = y_train
                self.__data["x_test"] = x_test
                self.__data["y_test"] = y_test
                self.__columns = x.columns
                result = True
        return result
