""" Class to implement Google analytics API """

import os.path
import googleapiclient.discovery
import pandas
from oauth2client.service_account import ServiceAccountCredentials


class GoogleAnalytics:
    """ Class to manage the Google Analytics API and the data fetched """
    service = None
    response = None
    data_frame = None
    DATE_RANGE = [{'startDate': '90daysAgo', 'endDate': 'today'}]

    def __init__(self, scope, key_file_location, view_id, json):
        """ CTor """
        self.scope = scope
        self.key_file_location = key_file_location
        self.view_id = view_id
        self.json = json

    def set_response(self, response):
        """ Setter for response """
        if 'reports' in response:
            self.response = response
            return True
        else:
            print('ERROR response does NOT contain reports field!')
            return False

    def get_data_frame(self):
        """ Getter for pandas data frame """
        return self.__response2panda_frame()

    def init_service(self):
        """ Method to test if an connection can be established """
        # Check if key_file_location is a valid file
        if not os.path.exists(self.key_file_location):
            self.print_parameters()
            return False

        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            self.key_file_location,
            self.scope)

        self.service = googleapiclient.discovery.build('analyticsreporting', 'v4',
                                                       credentials=credentials)
        return True

    def fetch_response(self):
        """ Function to fetch response from initialized service """
        if self.service:
            self.json['reportRequests'][0]['viewId'] = self.view_id
            self.set_response(self.service.reports().batchGet(body=self.json).execute())
            return True

        print("Service has not been initialized!")
        return False

    def print_parameters(self):
        """ Function to print the parameters parsed to the object """
        line_length = 100
        print('-' * line_length)
        print('Parameters:')
        print('Scope =', self.scope)
        print('Key file =', self.key_file_location)
        print('View ID =', self.view_id)
        print('-' * line_length)

    def __response2panda_frame(self):
        """ Method to convert the response class variable to pandas frame """
        tmp_list = []
        # get report data
        for report in self.response.get('reports', []):
            # set column headers
            column_header = report.get('columnHeader', {})
            dimension_headers = column_header.get('dimensions', [])
            metric_headers = column_header.get('metricHeader', {}).get('metricHeaderEntries', [])
            rows = report.get('data', {}).get('rows', [])

            for row in rows:
                # create dict for each row
                tmp_dict = {}
                dimensions = row.get('dimensions', [])
                date_range_values = row.get('metrics', [])

                # fill dict with dimension header (key) and dimension value (value)
                for header, dimension in zip(dimension_headers, dimensions):
                    tmp_dict[header] = dimension

                # fill dict with metric header (key) and metric value (value)
                for i, values in enumerate(date_range_values):
                    for metric, value in zip(metric_headers, values.get('values')):
                        # set int as int, float a float
                        if ',' in value or '.' in value:
                            tmp_dict[metric.get('name')] = float(value)
                        else:
                            tmp_dict[metric.get('name')] = int(value)

                tmp_list.append(tmp_dict)

        df = pandas.DataFrame(tmp_list)
        return df
