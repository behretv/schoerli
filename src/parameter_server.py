"""
Server to add new samples, get and optimize parameters
"""


class ParameterServer:
    """
    Defines a server class which handles all parameters
    """

    def __init__(self, param_struct):
        print('\nCreating parameter server...')
        self.param_struct = param_struct
        self.sample = 0

    def add_sample(self, sample):
        """ Function to add samples """
        self.sample = sample

    def get_parameter(self, param_id):
        """ Getter for specific parameter """
        return self.param_struct[param_id]
