""" Class to predict the amount of visitors of www.schoerli.de """

import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn import metrics

from .data_handler import DataHandler


class VisitorPrediction:
    """ Class which predicts the amount of visitors from the data gathered via GoogleAnalytics class """

    def __init__(self, data):
        """ CTor """
        self.__data_handler = DataHandler(data)
        self.__predictions = self.__linear_regression()

    @property
    def data_handler(self):
        assert self.__data_handler is not None
        return self.__data_handler

    @property
    def predictions(self):
        return self.__predictions

    def plot_raw_data(self):
        if self.data_handler.time is not None and "ga:users" in self.data_handler.data_frame:
            plt.plot_date(self.data_handler.time, self.data_handler.data_frame["ga:users"])

            if self.predictions is not None:
                x = self.data_handler.time[self.predictions[0]["ga:date"]]
                y = self.predictions[1]
                plt.plot_date(x, y, label="Fitted line", linestyle="-")

            plt.title("Linear Regression Result")
            plt.legend()
            return True
        return False

    def __linear_regression(self):
        if self.data_handler.split_data():
            # Fit the linear model into the data using the training samples
            lm = LinearRegression()
            lm.fit(self.data_handler.x_train, self.data_handler.y_train)

            # Coefficient data frame
            cdf = pd.DataFrame(lm.coef_, self.data_handler.columns, columns=["Coefficients"])
            print(cdf)

            # Check the prediction
            prediction = lm.predict(self.data_handle.rx_test)
            print("MSE:", metrics.mean_squared_error(self.data_handle.y_test, prediction))

            return [self.data_handle.x_test, prediction]
        return None
