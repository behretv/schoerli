#!/bin/bash

# Static code analysis
echo "#1 STATIC ANALYSIS:"
echo "#1.1 falke8:"
flake8 --max-line-length=120 src/*.py
echo "#1.2 mypy:"
mypy --ignore-missing-imports src/*.py
echo "#1.3 pylint:"
pylint -d C0301 -d E1101 src/*.py

# Testing: unit tests & coverage
echo "#2 TESTING:"
echo "#2.1 pytest:"
pytest
echo "#2.2 pytest-cov:"
pytest --cov=src tests/