"""
This file tests the GoogleAnalytics class
"""

import json
import numpy
import pandas
import pytest

from resources import resource_manager
from src import google_analytics


@pytest.fixture
def google_api():
    scopes = ['https://www.googleapis.com/auth/analytics.readonly']
    key_file_location = resource_manager.ResourceManager().get_client_secrets()
    view_id = '216480310'
    with open(resource_manager.ResourceManager().get_report_date_table()) as json_file:
        data = json.load(json_file)
    return google_analytics.GoogleAnalytics(scopes, key_file_location, view_id, data)


@pytest.mark.parametrize(
    "test_input, expected",
    [(resource_manager.ResourceManager().get_client_secrets(), True),
     (resource_manager.ResourceManager().get_wrong_client_secrets(), True),
     ("", False),
     ]
)
def test_server_init(test_input, expected):
    """ Testing the server initialization """
    scopes = ['https://www.googleapis.com/auth/analytics.readonly']
    key_file_location = test_input
    view_id = '216480310'
    with open(resource_manager.ResourceManager().get_report_date_table()) as json_file:
        data = json.load(json_file)
    google_analytics_api = google_analytics.GoogleAnalytics(scopes, key_file_location, view_id, data)
    assert google_analytics_api.init_service() == expected


@pytest.mark.trylast
def test_fetch_response(google_api):
    # 1 Test: service has not been init -> FALSE
    assert not google_api.fetch_response()

    # 2 Test: Init has been executed -> TRUE
    assert google_api.init_service()
    assert google_api.fetch_response()


@pytest.mark.parametrize(
    "test_input, expected",
    [
        ({'reports': 1}, True),
        ({'text': 1}, False)
    ]
)
def test_set_response(test_input, expected, google_api):
    assert google_api.set_response(test_input) == expected


@pytest.mark.parametrize(
    "test_input, expected",
    [
        (
                {'reports': [{'columnHeader': {'dimensions': ['ga:date'], 'metricHeader': {
                    'metricHeaderEntries': [{'name': 'ga:users', 'type': 'INTEGER'}]}}, 'data': {
                    'rows': [{'dimensions': ['20200424'], 'metrics': [{'values': ['28']}]},
                             {'dimensions': ['20200425'], 'metrics': [{'values': ['27']}]},
                             {'dimensions': ['20200723'], 'metrics': [{'values': ['9']}]}],
                    'totals': [{'values': ['1096']}],
                    'rowCount': 91, 'minimums': [{'values': ['1']}], 'maximums': [{'values': ['53']}]}}]
                 }, pandas.DataFrame(numpy.array([[20200424, 28], [20200425, 27], [20200723, 9]]),
                                     columns=['ga:date', 'ga:users'])
        ),
        (
                {'reports': [{'columnHeader': {'dimensions': ['ga:date'], 'metricHeader': {
                    'metricHeaderEntries': [{'name': 'ga:users', 'type': 'INTEGER'}]}}, 'data': {
                    'rows': [{'dimensions': ['1'], 'metrics': [{'values': ['2.8']}]},
                             {'dimensions': ['1'], 'metrics': [{'values': ['2.7']}]},
                             {'dimensions': ['2'], 'metrics': [{'values': ['9']}]}],
                    'totals': [{'values': ['1096']}],
                    'rowCount': 91, 'minimums': [{'values': ['1']}], 'maximums': [{'values': ['53']}]}}]
                 }, pandas.DataFrame(numpy.array([[1, 2.8], [1, 2.7], [1, 9]]),
                                     columns=['ga:date', 'ga:users'])
        )
    ]
)
def test_response2panda_frame(test_input, expected, google_api):
    google_api.set_response(test_input)
    frame = google_api.get_data_frame()
    assert frame.ndim == expected.ndim
    assert all([a == b for a, b in zip(frame, expected)])
