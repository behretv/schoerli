"""
This file tests the parameter server class
"""
import pytest

import src.parameter_server as ps


@pytest.mark.parametrize(
    "test_input, expected",
    [([1, 2, 3], [1, 2, 3])]
)
def test_initialization(test_input, expected):
    param_struct = {"theta": test_input}
    server = ps.ParameterServer(param_struct)
    server.add_sample(1)
    assert server.get_parameter("theta") == expected
