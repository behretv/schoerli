""" This file tests the resource manager """

import pytest
import os.path
from resources.resource_manager import ResourceManager


@pytest.mark.parametrize(
    "test_input, expected",
    [(ResourceManager().get_client_secrets(), True),
     (ResourceManager().get_wrong_client_secrets(), True),
     (ResourceManager().get_report_date_table(), True),
     (ResourceManager().get_report_user_table(), True),
     ("this_file_does_not_exist", False),
     ]
)
def test_file_parsing(test_input, expected):
    """ Testing the server initialization """
    assert os.path.isfile(test_input) == expected

