""" Testing the VisitorPrediction class """

import pytest
import pandas as pd
import numpy as np
from src.visitor_prediction import VisitorPrediction


@pytest.mark.parametrize(
    "test_input, expected",
    [
        (
            pd.DataFrame(
                np.array([[20200501, 1], [20200502, 2], [20200503, 3]]),
                columns=["ga:date", "ga:users"],
            ),
            True,
        ),
        (
            pd.DataFrame(
                np.array([[1, 2.8], [1, 2.7], [1, 9]]),
                columns=["ga:date", "ga:users"],
            ),
            True,
        ),
        (pd.DataFrame(np.array([1, 2, 3]), columns=["ga:date"]), False),
        (pd.DataFrame(np.array([1, 2, 3]), columns=["ga:users"]), False),
        (
            pd.DataFrame(np.array([1, 2, 3]), columns=["some_random_string"]),
            False,
        ),
    ],
)
def test_server_init(test_input, expected):
    """ Testing the server initialization """
    visitor_prediction = VisitorPrediction(test_input)
    assert visitor_prediction.plot_raw_data() == expected
